package Models;

import Logic.AnimalFactory;
import Models.Sellable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Webshop implements Serializable {

    private static List<Sellable> sellableList = new ArrayList<>();

    public List<Sellable> getSellableList() {
        return Collections.unmodifiableList(sellableList);
    }

    public void addSellable(Sellable sellable) {
        sellableList.add(sellable);
    }

    public void buySellable(Sellable sellable) {
        sellableList.remove(sellable);
    }

    public Webshop(){
    }

    public void update(Object sellable){
        sellableList.clear();
        sellableList.addAll((Collection<? extends Sellable>) sellable);
    }
}
