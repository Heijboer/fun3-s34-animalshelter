package Models;

import Models.Animal;
import Models.Cat;
import Models.Dog;
import Models.Gender;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Reservation implements Serializable {

    public List<Animal> animals = new ArrayList<Animal>();

    public List<Webshop> webshops = new ArrayList<>();

    public ObservableList<Animal> getAnimals() {
        return FXCollections.observableArrayList(animals);
    }

    public void newAnimal(Animal animal){
        this.animals.add(animal);

        for (Webshop webshop : webshops){
            webshop.update(this.animals);
        }
    }

    public void addObserver(Webshop webshop){
        this.webshops.add(webshop);
    }

    public void removeObserver(Webshop webshop) {
        this.webshops.remove(webshop);
    }

}
