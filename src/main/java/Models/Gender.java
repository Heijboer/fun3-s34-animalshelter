package Models;

public enum Gender{
        Male("Male"),
        Female("Female");

    private String label;

    Gender(String label){
        this.label = label;
    }

    public String toString(){
        return label;
    }
}

