package Models;

import Logic.CatPriceCalculator;

public class Cat extends Animal {

    private String badHabits;

    public Cat(String name, Gender gender, String badHabits){
        super(name, gender, new CatPriceCalculator());
        this.badHabits = badHabits;
    }

    public String getBadHabits(){return badHabits;}
    public void setBadHabits(String value){badHabits = value;}

    @Override
    public String toString(){
        return super.toString() + String.format(", bad habits: %s", this.badHabits.toLowerCase());
    }
}
