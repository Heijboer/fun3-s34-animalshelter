package Models;

public class Product implements Sellable {

    public String productInfo;
    public float price;
    public String name;

    public Product(String name, String productInfo, float price){
        this.name = name;
        this.productInfo = productInfo;
        this.price = price;
    }

    public String getProductInfo(){return productInfo;}
    private void setProductInfo(String value){productInfo = value;}

    @Override
    public String getName() {return name;}

    @Override
    public float getPrice() {return price;}

    @Override
    public java.lang.String toString() {
        return String.format("%s, %s", this.name, this.productInfo);
    }
}
