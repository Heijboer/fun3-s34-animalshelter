package Models;
import Logic.DateCalculator;
import Logic.DogPriceCalculator;

import java.util.Date;

public class Dog extends Animal {

    private Date lastWalk;
    private boolean needWalk;

    public Dog(String name, Gender gender) {
        super(name, gender, new DogPriceCalculator());
        this.lastWalk = new Date();
    }

    public Date getLastWalk(){
        return lastWalk;
    }
    public void setLastWalk(Date value){
        lastWalk = value;
    }

    public boolean getNeedWalk(){
        if (DateCalculator.getDifferenceDays(new Date(), this.lastWalk) > 0){
            return true;
        }
        return false;
    }
    public void setNeedWalk(boolean value){
        needWalk = value;
    }
}
