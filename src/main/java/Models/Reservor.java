package Models;

import java.util.Date;

public class Reservor {

    private String name;
    private Date reservedAt;

    public Reservor(String name, Date reservedAt){
        this.name = name;
        this.reservedAt = reservedAt;
    }

    public String getName(){
        return name;
    }
    public void setName(String value){
        name = value;
    }
    public Date getReservedAt(){
        return reservedAt;
    }
    public void setReservedAt(Date value){
        reservedAt = value;
    }
}
