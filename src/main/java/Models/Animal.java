package Models;

import Logic.AnimalPriceCalculator;

import java.io.Serializable;
import java.util.Date;

public abstract class Animal implements Sellable, Serializable {

    private AnimalPriceCalculator animalPriceCalculator;
    private String name;
    private Gender gender;
    private Reservor reservedBy;
    private float price;

    public Animal(String name, Gender gender, AnimalPriceCalculator animalPriceCalculator){
        this.name = name;
        this.gender = gender;
        this.animalPriceCalculator = animalPriceCalculator;
    }

    public AnimalPriceCalculator getAnimalPriceCalculator(){
        return animalPriceCalculator;
    }

    public float getPrice(){
        return price;
    };

    public void setPrice(float value){
        this.price = value;
    }

    public String getName(){return name;};
    public void setName(String value){name = value;}
    public Gender getGender(){return gender;}
    public void setGender(Gender value){gender = value;}
    public Reservor getReservedBy(){return reservedBy;}
    public void setName(Reservor value){reservedBy = value;}

    public boolean reserve(String reservedBy){
        if (this.reservedBy == null){
            this.reservedBy = new Reservor(reservedBy, new Date());
            return true;
        }
        return false;
    }

    public String toString(){
        String reserved = "not reserved";
        if (this.reservedBy != null) {
            reserved = String.format("reserved by %s", this.reservedBy.getName());
        }
        return String.format("%s, %s, %s", this.name, this.gender, reserved);
    }
}

