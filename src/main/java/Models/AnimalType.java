package Models;

public enum AnimalType {
    Dog("Dog"),
    Cat("Cat");

    private String label;

    AnimalType(String label){
        this.label = label;
    }

    public String toString(){
        return label;
    }
}
