package Models;

public interface Sellable {

    String getName();

    float getPrice();
}
