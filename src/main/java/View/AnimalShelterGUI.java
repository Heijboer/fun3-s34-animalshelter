package View;

import Data.*;
import Logic.AnimalFactory;
import Models.*;
import View.utilities.AlertBox;
import View.utilities.ConfirmBox;
import com.sun.tools.javac.Main;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class AnimalShelterGUI extends Application {

    private Reservation reservation = new Reservation();
    private Storage animalStorage;
    private Stage window;
    private ListView<Animal> animalsListView;
    private Button reserveAnimalButton;
    private TextField animalNameTextField;
    private TextField badHabitsTextField;
    private TextField reserveNameTextField;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {

        // Stage settings
        window = stage;
        window.setTitle("AnimalShelter");

        // Grid layout
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);
        //grid.setGridLinesVisible(true);

        // Species label
        Label speciesLabel = new Label("Species");
        GridPane.setConstraints(speciesLabel, 0, 0, 2, 1);
        // Species Combobox
        ComboBox<AnimalType> speciesComboBox = new ComboBox<>();
        ObservableList<AnimalType> animalTypes = FXCollections.observableArrayList(AnimalType.values());
        speciesComboBox.setItems(animalTypes);
        speciesComboBox.setValue(AnimalType.Dog);
        GridPane.setConstraints(speciesComboBox, 0, 1, 2, 1);

        // Animal Name Label
        Label animalNameLabel = new Label("Name");
        GridPane.setConstraints(animalNameLabel, 0, 2, 2, 1);
        // Animal Name Textfield
        animalNameTextField = new TextField();
        animalNameTextField.setPromptText("Name");
        GridPane.setConstraints(animalNameTextField, 0, 3, 2 , 1);

        // Gender Label
        Label genderLabel = new Label("Gender");
        GridPane.setConstraints(genderLabel, 0, 4, 2 , 1);
        // Gender Radiobutton
        ToggleGroup pickGender = new ToggleGroup();

        RadioButton maleRadioButton = new RadioButton("Male");
        maleRadioButton.setToggleGroup(pickGender);
        maleRadioButton.setSelected(true);
        GridPane.setConstraints(maleRadioButton, 0, 5, 1, 1);

        RadioButton femaleRadioButton = new RadioButton("Female");
        femaleRadioButton.setToggleGroup(pickGender);
        GridPane.setConstraints(femaleRadioButton, 1, 5,1 ,1);

        // Bad habits Label
        Label badHabitsLabel = new Label("Bad habits");
        GridPane.setConstraints(badHabitsLabel, 0, 6, 2, 1);
        // Bad habits Textfield
        badHabitsTextField = new TextField();
        badHabitsTextField.setPromptText("Ex: \"Scratches couch\" ");
        badHabitsTextField.setDisable(true);
        GridPane.setConstraints(badHabitsTextField, 0, 7, 2, 1);

        // Add Animal Button
        Button addAnimalButton = new Button("Add animal");
        GridPane.setConstraints(addAnimalButton, 0, 8, 2, 1);

        // Animals Label
        Label animalsLabel = new Label("Animals");
        GridPane.setConstraints(animalsLabel, 3, 0, 2 , 1);
        // Animals List
        animalsListView = new ListView<Animal>();
        GridPane.setConstraints(animalsListView, 3, 1, 4, 3);

        // Reserve Animal Label
        Label reserveLabel = new Label("Reserve animal");
        GridPane.setConstraints(reserveLabel, 3, 4, 2, 1);
        // Reserve Name Label
        Label reserveNameLabel = new Label("Name");
        GridPane.setConstraints(reserveNameLabel, 3, 5, 1, 1);
        // Reserve Name Textfield
        reserveNameTextField = new TextField();
        reserveNameTextField.setPromptText("Name");
        GridPane.setConstraints(reserveNameTextField, 4, 5, 1, 1);
        // Reserve Animal Button
        reserveAnimalButton = new Button("Reserve selected Animal");
        reserveAnimalButton.setDisable(true);
        GridPane.setConstraints(reserveAnimalButton, 5, 5, 1, 1);

        // Price Label
        Label priceLabel = new Label("Price: ");
        GridPane.setConstraints(priceLabel, 4, 7, 2, 1);

        // Store Button
        Button storeButton = new Button("Store");
        GridPane.setConstraints(storeButton, 5, 7, 2, 1);



        // Set grid
        grid.getChildren().addAll(speciesLabel, speciesComboBox, animalNameLabel, animalNameTextField, genderLabel, maleRadioButton, femaleRadioButton,
                badHabitsLabel, badHabitsTextField, addAnimalButton, animalsLabel, animalsListView, reserveLabel, reserveNameLabel, reserveNameTextField,
                reserveAnimalButton, priceLabel, storeButton);
        Scene scene = new Scene(grid, 550, 370);
        window.setScene(scene);
        window.show();

        // Initialize Items
        this.animalStorage = new AnimalStorage();

        try {
            this.reservation = this.animalStorage.getSavedReservations();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        animalsListView.setItems(this.reservation.getAnimals());

        // Event handlers

        window.setOnCloseRequest(e -> {
            e.consume();
            closeProgram();
        });

        addAnimalButton.setOnAction(e -> {
            Gender gender = maleRadioButton.isSelected() ? Gender.Male : Gender.Female;
            AnimalType animalType = speciesComboBox.getValue();
            String name = animalNameTextField.getText();
            String badHabits = badHabitsTextField.getText();

            Animal animal = AnimalFactory.createAnimal(animalType, name, gender, badHabits);

            Webshop observer = new Webshop();
            reservation.addObserver(observer);
            reservation.newAnimal(animal);

            this.animalStorage = new AnimalStorage();

            try {
                animalStorage.saveReservations(reservation);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            refreshControls();
        });

        speciesComboBox.setOnAction(e -> {
            if (speciesComboBox.getValue() == AnimalType.Cat){
                badHabitsTextField.setDisable(false);
            }
            else{
                badHabitsTextField.setDisable(true);
            }

            // Initialize
            try {
                this.reservation = this.animalStorage.getSavedReservations();
            } catch (IOException | ClassNotFoundException a) {
                a.printStackTrace();
            }
        });

        animalsListView.setOnMouseClicked(e -> {
            if (animalsListView.getSelectionModel().getSelectedItem() != null){
                priceLabel.setText("Price: " + "$"+animalsListView.getSelectionModel().getSelectedItem().getPrice());
                reserveAnimalButton.setDisable(false);
            }
            else{
                reserveAnimalButton.setDisable(true);
            }
        });

        reserveAnimalButton.setOnAction(e -> {
            Animal animal = (Animal)animalsListView.getSelectionModel().getSelectedItem();

            if (animal != null){
                animal.reserve(reserveNameTextField.getText());
                refreshControls();
            }
        });

        storeButton.setOnAction(e -> {
            StoreGUI.display();
        });
    }

    private void refreshControls(){
        animalsListView.getItems().clear();

        for (Animal animal : reservation.getAnimals()){
            animalsListView.getItems().add(animal);
        }
        reserveAnimalButton.setDisable(true);
        animalsListView.getSelectionModel().clearSelection();
        animalNameTextField.clear();
        reserveNameTextField.clear();
        badHabitsTextField.clear();
    }

    private void closeProgram(){
        boolean answer = ConfirmBox.display("Exit", "Are you sure you want to exit?");
        if (answer){
            System.out.println("yaa");
            window.close();
        }
    }
    }
