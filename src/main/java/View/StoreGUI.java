package View;

import Models.Animal;
import Models.AnimalType;
import Models.Sellable;
import Models.Webshop;
import View.utilities.AlertBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class StoreGUI {

    private static ListView<Sellable> sellableListView;
    private static Button buySellableButton;
    private static Webshop webshop = new Webshop();
    private static Label priceLabel;

    public static void display(){

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Store");
        window.setMinWidth(550);
        window.setMinHeight(370);

        // Sellables Label
        Label sellableLabel = new Label("Products");
        GridPane.setConstraints(sellableLabel, 3, 0, 2 , 1);
        // Sellables List
        sellableListView = new ListView<Sellable>();
        for (Sellable sellable : webshop.getSellableList()){
            sellableListView.getItems().add(sellable);
        }
        GridPane.setConstraints(sellableListView, 3, 1, 3, 3);

        // Buy sellable Label
        Label buySellableLabel = new Label("Buy product");
        GridPane.setConstraints(buySellableLabel, 3, 4, 2, 1);
        // Buy sellable Button
        buySellableButton = new Button("Buy selected product");
        buySellableButton.setDisable(true);
        GridPane.setConstraints(buySellableButton, 5, 4, 1, 1);

        // Price Label
        priceLabel = new Label("Price: ");
        GridPane.setConstraints(priceLabel, 6, 2, 2, 1);

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);

        grid.getChildren().addAll(sellableLabel, sellableListView, buySellableLabel, buySellableButton, priceLabel);
        Scene scene = new Scene(grid, 550, 370);
        window.setScene(scene);
        window.show();

        sellableListView.setOnMouseClicked(e -> {
            if (sellableListView.getSelectionModel().getSelectedItem() != null){
                priceLabel.setText("Price: " + "$"+sellableListView.getSelectionModel().getSelectedItem().getPrice());
                buySellableButton.setDisable(false);
            }
            else{
                buySellableButton.setDisable(true);
            }
        });

        buySellableButton.setOnAction(e -> {
            Sellable sellable = (Sellable) sellableListView.getSelectionModel().getSelectedItem();

            if (sellable != null){
                webshop.buySellable(sellable);
                refreshControls();
            }
        });
    }

    private static void refreshControls(){
        sellableListView.getItems().clear();

        for (Sellable sellable : webshop.getSellableList()){
            sellableListView.getItems().add(sellable);
        }

        buySellableButton.setDisable(true);
        sellableListView.getSelectionModel().clearSelection();
        priceLabel.setText("Price: ");
    }


}
