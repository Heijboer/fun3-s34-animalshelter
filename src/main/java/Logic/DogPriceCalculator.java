package Logic;

import Data.AnimalStorage;
import Models.*;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DogPriceCalculator implements AnimalPriceCalculator, Serializable {

    private final float maxPrice = 500;
    private final float minPrice = 50;

    @Override
    public float calculatePrice(Animal animal) {

        AnimalStorage animalStorage = new AnimalStorage();

        List<Dog> dogs = new ArrayList<>();
        float price = maxPrice;

        try {
            for (Sellable sellable : animalStorage.getSavedReservations().getAnimals()){
                if (sellable instanceof Dog ){
                    dogs.add((Dog)sellable);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
        }

        price = maxPrice - dogs.size() * 50;

        if (price < minPrice){
            price = minPrice;
        }

        return price;
    }
}
