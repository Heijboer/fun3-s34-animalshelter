package Logic;

import java.util.Date;

public class DateCalculator {

    public static long getDifferenceDays(Date date1, Date date2){
        long difference = date1.getTime() - date2.getTime();
        long differenceInDays = (difference / (1000*60*60*24));

        return differenceInDays;
    }
}