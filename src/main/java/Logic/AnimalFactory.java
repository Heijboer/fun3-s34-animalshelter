package Logic;

import Models.*;

public class AnimalFactory {

    public static Animal createAnimal(AnimalType animalType, String name, Gender gender, String badHabits){
        Animal animal;

        switch(animalType){
            case Dog:
                animal = new Dog(name, gender);
                break;
            case Cat:
                animal = new Cat(name, gender, badHabits);
                break;
            default:
                throw new NullPointerException();
        }

        float price = animal.getAnimalPriceCalculator().calculatePrice(animal);
        animal.setPrice(price);
        return animal;
    }
}
