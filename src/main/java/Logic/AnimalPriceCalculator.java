package Logic;
import Models.Animal;

public interface AnimalPriceCalculator {

    public float calculatePrice(Animal animal);

}
