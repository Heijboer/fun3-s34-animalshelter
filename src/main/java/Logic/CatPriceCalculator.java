package Logic;

import Models.Animal;
import Models.Cat;
import Models.Webshop;

import java.io.Serializable;

public class CatPriceCalculator implements AnimalPriceCalculator, Serializable {

    Webshop webshop = new Webshop();
    private final float maxPrice = 350;
    private final float minPrice = 35;

    @Override
    public float calculatePrice(Animal animal) {

        float price = maxPrice;
        Cat cat = (Cat) animal;
        for (char ch:  cat.getBadHabits().toCharArray()){
            price = price - 20;
        }

        if (price < minPrice){
            price = minPrice;
        }
        return price;

    }
}
