package Data;

import Models.Reservation;

import java.io.*;

public interface Storage {

    void saveReservations(Reservation reservation) throws IOException;

    Reservation getSavedReservations() throws IOException, ClassNotFoundException;
}
