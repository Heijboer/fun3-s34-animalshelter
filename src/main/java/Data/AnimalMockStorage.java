package Data;

import Models.Reservation;

import java.io.*;

public class AnimalMockStorage implements Storage {

    private final File shelterFile = new File("reservationsMock.txt");

    private void serializeObject(Object o) throws IOException {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(shelterFile);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(o);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private <T> T deserializeData(Class<T> cls, String path) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(path);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        T object = cls.cast(objectInputStream.readObject());
        objectInputStream.close();
        fileInputStream.close();
        return object;
    }

    @Override
    public void saveReservations(Reservation reservation) throws IOException {
        serializeObject(reservation);
    }

    @Override
    public Reservation getSavedReservations() throws IOException, ClassNotFoundException {
        return deserializeData(Reservation.class, shelterFile.getPath());
    }
}
