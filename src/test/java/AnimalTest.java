import Logic.AnimalFactory;
import Models.*;
import org.junit.Assert;
import org.junit.Test;

public class AnimalTest {

    private Animal cat;

    public AnimalTest()
    {
        this.cat = new Cat("Ms.Meow", Gender.Male, "Loud");
    }

    @Test
    public void TestConstructor() {
        Assert.assertEquals("Ms.Meow", this.cat.getName());
        Assert.assertEquals(Gender.Male, this.cat.getGender());
        Assert.assertNull(this.cat.getReservedBy());
    }

    @Test
    public void TestReservation() {
        Assert.assertNull(this.cat.getReservedBy());
        Assert.assertTrue(this.cat.reserve("John Doe"));
        Assert.assertNotNull(this.cat.getReservedBy());
        Assert.assertEquals("John Doe", this.cat.getReservedBy().getName());
        Assert.assertFalse(this.cat.reserve("Jane Doe"));
    }

    @Test
    public void TestGetPriceCat() {
        Animal testCat = AnimalFactory.createAnimal(AnimalType.Cat, "Mr. Woof", Gender.Male, "Loud");
        Assert.assertEquals(350 - 4*20, testCat.getPrice(), 1e-15);
    }

/*    @Test
    public void TestGetPriceDog(){
        Reservation reservation = new Reservation();
        reservation.newAnimal(AnimalFactory.createAnimal(AnimalType.Dog, "Woofer", Gender.Male, ""));
        reservation.newAnimal(AnimalFactory.createAnimal(AnimalType.Dog, "Mr. Woof", Gender.Male, ""));
        Animal testDog = AnimalFactory.createAnimal(AnimalType.Dog, "Ms. Bark", Gender.Female, "");
        Assert.assertEquals(500 - 2 * 50, testDog.getPrice(), 1e-15);
    }*/
}