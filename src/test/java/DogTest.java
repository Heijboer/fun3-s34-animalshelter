import Models.Dog;
import Models.Gender;
import Models.Webshop;
import org.junit.Assert;
import org.junit.Test;

public class DogTest {

    Webshop webshop = new Webshop();

    private Dog dog;
    public DogTest(){
        this.dog = new Dog("Sgt. Woof", Gender.Male);
    }

    @Test
    public void TestConstructor() {
        Assert.assertEquals("Sgt. Woof", this.dog.getName());
        Assert.assertEquals(Gender.Male, this.dog.getGender());
        Assert.assertNull(this.dog.getReservedBy());
    }

    @Test
    public void TestReservation() {
        Assert.assertNull(this.dog.getReservedBy());
        Assert.assertTrue(this.dog.reserve("John Doe"));
        Assert.assertNotNull(this.dog.getReservedBy());
        Assert.assertEquals("John Doe", this.dog.getReservedBy().getName());
        Assert.assertFalse(this.dog.reserve("Jane Doe"));
    }
}