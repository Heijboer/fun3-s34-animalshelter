import Logic.AnimalFactory;
import Models.AnimalType;
import Models.Gender;
import Models.Reservation;
import org.junit.Assert;
import org.junit.Test;

public class ReservationTest {

    Reservation reservation;

    public ReservationTest() {
        reservation = new Reservation();
    }

    @Test
    public void TestNewCat() {
        Assert.assertEquals(0, reservation.animals.size());
        this.reservation.newAnimal(AnimalFactory.createAnimal(AnimalType.Cat,"Ms. Meow", Gender.Female, "Scratches couch"));
        Assert.assertEquals(1, reservation.animals.size());

    }

    @Test
    public void TestNewDog() {
        Assert.assertEquals(0, reservation.animals.size());
        this.reservation.newAnimal(AnimalFactory.createAnimal(AnimalType.Dog,"Sgt. Woof", Gender.Male, ""));
        Assert.assertEquals(1, reservation.animals.size());
    }

}